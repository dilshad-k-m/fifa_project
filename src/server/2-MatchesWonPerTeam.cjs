module.exports = function MatchesWonPerTeam(WorldCupMatches) {
    let ValidMatches=WorldCupMatches.filter(val=>val.Year!=="")
  let TeamsWon = ValidMatches.reduce((accum, current) => {
      if (current["Home Team Goals"] > current["Away Team Goals"]) {
        if (accum[current["Home Team Name"]]) {
          accum[current["Home Team Name"]]++;
        } else {
          accum[current["Home Team Name"]] = 1;
        }
      } else if (current["Home Team Goals"] < current["Away Team Goals"]) {
        if (accum[current["Away Team Name"]]) {
          accum[current["Away Team Name"]]++;
        } else {
          accum[current["Away Team Name"]] = 1;
        }
      } else {
        if (current["Win conditions"] !== " ") {
          let x = current["Win conditions"].replace(/[^0-9]/g, "");
          if (Number(x[0]) > Number(x[1])) {
            if (accum[current["Home Team Name"]]) {
              accum[current["Home Team Name"]]++;
            } else {
              accum[current["Home Team Name"]] = 1;
            }
          } else {
            if (accum[current["Away Team Name"]]) {
              accum[current["Away Team Name"]]++;
            } else {
              accum[current["Away Team Name"]] = 1;
            }
          }
        }
      }
    return accum;
  }, {});

  return TeamsWon;
}
