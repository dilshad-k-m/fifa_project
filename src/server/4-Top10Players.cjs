module.exports = function TopTenPlayers(WorldCupPlayers) {
  let PlayersGoalsProbability = [];
  let PlayersGoals = WorldCupPlayers.reduce((accum, current) => {
    if (current["Event"].includes("G")) {
      if (accum[current["Player Name"]]) {
        accum[current["Player Name"]] += 1;
      } else {
        accum[current["Player Name"]] = 1;
      }
    }
    return accum;
  }, {});
  let PlayersTotalMatches = WorldCupPlayers.reduce((accum, current) => {
    if (current["Player Name"] in accum) {
      accum[current["Player Name"]]++;
    } else {
      accum[current["Player Name"]] = 1;
    }
    return accum;
  }, {});
  for (Player in PlayersGoals) {
      PlayersGoalsProbability.push([
        Player,
        PlayersGoals[Player] / PlayersTotalMatches[Player],
      ]);
  }
  PlayersGoalsProbability.sort((p, q) => {
    return q[1] - p[1];
  });
  return PlayersGoalsProbability.slice(0, 10);
};
