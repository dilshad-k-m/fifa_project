module.exports = function RedcardsCount(WorldCupMatches, WorldCupPlayers) {
  let PlayerswithRedcards = WorldCupPlayers.reduce((accum, current) => {
    if (current.Event.includes("R")) {
      accum[current.MatchID] = current["Team Initials"];
    }
    return accum;
  }, {});
  let WorldCupMatches2014 = WorldCupMatches.filter(val => val.Year==2014 && val.MatchID in PlayerswithRedcards)
  let Teamsredcardcount = WorldCupMatches2014.reduce((accum, current) => {
      if (
        PlayerswithRedcards[current.MatchID] === current["Home Team Initials"]
      ) {
        if (accum[current["Home Team Name"]]) {
          accum[current["Home Team Name"]]++;
        } else {
          accum[current["Home Team Name"]] = 1;
        }
      } else {
        if (accum[current["Away Team Name"]]) {
          accum[current["Away Team Name"]]++;
        } else {
          accum[current["Away Team Name"]] = 1;
        }
      }
    
    return accum;
  }, {});
  return Teamsredcardcount;
};
