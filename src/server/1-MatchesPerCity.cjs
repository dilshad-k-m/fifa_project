module.exports = function MatchesPerCity(WorldCupMatches) {
  let ValidMatches=WorldCupMatches.filter(val=>val.Year!=="")
  let CityandMatches = ValidMatches.reduce((accum, current) => {
    if (current.City in accum) {
      accum[current.City]++;
    } else {
      accum[current.City] = 1;
    }
    return accum;
  }, {});
  return CityandMatches;
};